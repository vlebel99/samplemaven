public class Main {
    public static void main(String[] args) {

        CountingMatches.matchingWordsByPhrase(ReadDate.readData().string1, ReadDate.readData().string2);

        CountingMatches.count(ReadDate.readData().string1, ReadDate.readData().word, ReadDate.readData().punctuationMarks);

        ProjectInformation.systemInformation();
    }
}
