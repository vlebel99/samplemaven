import entity.Strings;

import java.util.Arrays;
import java.util.HashMap;

public class CountingMatches {

    public static void matchingWordsByPhrase(String string1, String string2) {

        Strings strings = new Strings(string1, string2);

        System.out.println("Сравниваем две строки " + "\"" + strings.getString1() + "\"" + " и " + "\"" + strings.getString2() + "\".");

        strings.setSubStr1(strings.getString1().split(" "));
        strings.setSubStr2(strings.getString2().split(" "));

        for (String word : strings.getSubStr2()) {
            if (Arrays.asList(strings.subStr1).contains(word)) {
                System.out.println("Есть совпадения и это слово \"" + word + "\"");
            }
        }
        System.out.println();
    }

    public static void count(String string1, String word, String punctuationMark) {

        int count = 0;
        HashMap<String, Integer> wordToCount = new HashMap<>();
        Strings strings = new Strings(string1, word, punctuationMark);
        strings.setSubStr1(strings.getString1().split(" "));
        strings.setSubStr2(strings.getPunctuationMark().split(" "));

        System.out.println("Считаем сколько в строке " + "\"" + strings.getString1() + "\" встречается слов и знаков припинамя " + strings.getPunctuationMark());

        for (String w : strings.getSubStr1()) {
            if (!wordToCount.containsKey(w)) {
                wordToCount.put(w, 0);
            }
            wordToCount.put(w, wordToCount.get(w) + 1);
        }
        for (String w : wordToCount.keySet()) {
            if (w instanceof String || Arrays.asList(strings.subStr2).contains(w)){
                count = count + wordToCount.get(w);
            }
        }
        System.out.println(count);
        System.out.println();
    }
}
